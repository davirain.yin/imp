#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX_SIZE 300
struct StructS
{
    int n[MAX_SIZE];
    char s[MAX_SIZE][MAX_SIZE];
};
StructS fileS[MAX_SIZE];

char s_file_path[128];
char r_file_path[128];
char dot_file_path[128];


int hangshu_s()    //计算s文件行数
{
   // memset(flieS, 0, 10 * sizeof(StructS));
    //FILE* pf = fopen("D:/S.txt", "r"); // 打开文件
    FILE * pf = fopen(s_file_path, "r");
    char buf[1000];
    int lineCnt = 0;
    if (NULL == pf)
    {
        fprintf(stderr, "打开文件描述符失败\n");
        return -1;
    }
    while (fgets(buf, 1000, pf)) // fgets循环读取，直到文件最后，才会返回NULL
        lineCnt++; // 累计行数
    fclose(pf);
    printf("file S line count = %d\n", lineCnt);
    return lineCnt;
}
int hangshu_r()    //计算r文件行数
{
    //FILE* pf = fopen("D:/R.txt", "r"); // 打开文件
    FILE* pf = fopen(r_file_path, "r");
    char buf[1000];
    int lineCnt = 0;
    if (NULL == pf)
    {
        fprintf(stderr, "打开文件描述符失败\n");
        return -1;
    }
    while (fgets(buf, 1000, pf)) // fgets循环读取，直到文件最后，才会返回NULL
        lineCnt++; // 累计行数
    fclose(pf);
    printf("file R line count = %d\n", lineCnt);
    return lineCnt;
}
int  node_n(char name[], StructS array[]) {
    int S = hangshu_s();
    for (int i = 0; i < S; i++) {
        if (0 == strcmp(name, array->s[i])) {
            return array->n[i];
        }
    }  

}
void printpicture(const char* filename_dot) {       //生成dot文件


    ///// 以下是输出到heap_*.dot文件中，供用graphicvz生成png图片节点表示法用 /////
     //dot -Tpng ks.dot -o ks.png
    char file_name_ks[30] = "D:/R.txt";
    //FILE* fps = fopen("D:/S.txt", "r"); // 打开文件
    FILE * fps = fopen(s_file_path, "r");
    int i = 0;
    FILE* fp_dot = fopen(filename_dot, "w+");
    //FILE* fp_ks = fopen(file_name_ks, "r+");
    FILE* fp_ks = fopen(r_file_path, "r");
    if (NULL == fp_ks)
    {
        fprintf(stderr, "打开文件描述符失败\n");
        return;
    }
    if (NULL == fps)
    {
        fprintf(stderr, "打开文件描述符失败\n");
        return;
    }
    fprintf(fp_dot, "digraph g{\n");
    char str[MAX_SIZE] = { 0 };;
    fprintf(fp_dot, "node [shape = circle,height=.2];\n");
    int p = 0;
    char compare1[MAX_SIZE] = { 3 };
    char compare2[MAX_SIZE][MAX_SIZE] = { 0 };
    char compare3[MAX_SIZE] = { 3 };
    char compare4[MAX_SIZE][MAX_SIZE] = { 0 };
    while (fgets(str, MAX_SIZE, fps)) {    //读入每行数据


        str[strlen(str) - 1] = '\0';
        fprintf(fp_dot, "node%d[label=\"S%d\\n%s\"];\n", p, p, str);
        p++;
    }
    memset(fileS, 0, 10 * sizeof(StructS));
    int d = 0;
    int R = hangshu_r();
    int S = hangshu_s();
    while (fgets(compare1, R, fp_ks)) {          //把r.txt的内容封装到二维数组中

        compare1[strlen(compare1) - 1] = '\0';
        printf("r的内容为  %s\n", compare1);

        strcpy(compare2[d], compare1);
        d++;
    }
    d = 0;
    fseek(fps, 0L, SEEK_SET);
    while (fgets(compare3, MAX_SIZE, fps)) {          //把s.txt的内容封装到二维数组中
        compare3[strlen(compare3) -1] = '\0';
        printf("s的内容为%s\n", compare3);

        strcpy(compare4[d], compare3);
        d++;
    }

    for (int i = 0; i < S; i++) {                //把s.txt的内容封装到结构体数组中
        strcpy(fileS->s[i], compare4[i]);
        printf("s二维数组数组的内容为  %s\n", fileS->s[i]);
    }
    for (int i = 0; i < S; i++) {
        fileS->n[i] = i;
    }
    for (int i = 0; i < S; i++) {                //打印结构体
        printf("%d\n%s\n", fileS->n[i], fileS->s[i]);
    }

    int temp1 = 0;
    int temp2 = 0;
    for (int i = 0; i < R; i=i+2) {
        temp1 = node_n(compare2[i], fileS);
        temp2 = node_n(compare2[i+1], fileS);
        printf("temp1为  %d\n", temp1);
        printf("temp2为  %d\n", temp2);
        fprintf(fp_dot, " \"node%d\" ->\"node%d\";\n", temp1, temp2);

        
    }

        fprintf(fp_dot, "\n}\n");
        fclose(fp_ks);
        fclose(fp_dot);
  }
int main(int argc, char * argv[])
{
    printf("argc:%d\r\n%s\r\n%s\r\n%s\r\n", argc, argv[1], argv[2], argv[3]);
    memset(s_file_path, 0, sizeof(s_file_path));
    snprintf(s_file_path, sizeof(s_file_path), "%s", argv[1]);
    memset(r_file_path, 0, sizeof(r_file_path));
    snprintf(r_file_path, sizeof(r_file_path), "%s", argv[2]);    
    memset(dot_file_path, 0, sizeof(dot_file_path));
    snprintf(dot_file_path, sizeof(dot_file_path), "%s", argv[3]);
    //printpicture("KS.dot");
    printpicture(dot_file_path);
    return 0;
}