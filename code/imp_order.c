#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cJSON.h"

#define version 2021051202

#define MAX_BUF_SIZE	(256)
#define LABEL_FILE		"lable_file.txt"
#define ORDER_FILE		"order_file.txt"


typedef enum
{
	E_EXP_TYEP_SEQUENCE = 1,
	E_EXP_TYPE_BRANCH,
	E_EXP_TYPE_CIRCULATION,
}E_EXP_TYPE;

typedef struct _order_node_
{
	char label_start[8];
	char exp[32];
	char div;
	char label_end[8];
	struct _order_node_ *next;
}S_ORDER_NODE;

S_ORDER_NODE order_node_head;
char val[26] = {0};
int end_label = 0;


int add_node(S_ORDER_NODE **node , int start, int end, char *exp, E_EXP_TYPE exp_type)
{
	S_ORDER_NODE * node_temp = NULL;
	if(node == NULL || *node == NULL || start <= 0 || end <= 0 || exp == NULL )
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	node_temp = malloc(sizeof(S_ORDER_NODE));
	if(node_temp == NULL)
	{
		printf("[%s,%d],malloc fail\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	memset(node_temp, 0, sizeof(S_ORDER_NODE));
	switch(exp_type)
	{
		case E_EXP_TYEP_SEQUENCE:
		{
			if(!strstr(exp, "skip"))
			{	
				node_temp->div = exp[0];
				val[exp[0] - 'a'] = 1;
				snprintf(node_temp->exp, sizeof(node_temp->exp)-1, "%s", exp );
			}
		}break;
		case E_EXP_TYPE_BRANCH:
		{
			if(!(exp[0] == '_'))
				snprintf(node_temp->exp, sizeof(node_temp->exp)-1, "%s", exp );
		}break;
		case E_EXP_TYPE_CIRCULATION:
		{
			if(!(exp[0] == '_'))
				snprintf(node_temp->exp, sizeof(node_temp->exp)-1, "%s", exp );
		}break;
		default:
		{		
			printf("[%s,%d],unknow type:%d\r\n", __FUNCTION__, __LINE__, exp_type);
			free(node_temp);
			return -1;
		}
	}
	snprintf(node_temp->label_start, sizeof(node_temp->label_start), "L%d", start);
	if( end != end_label)
		snprintf(node_temp->label_end, sizeof(node_temp->label_end), "L%d", end);
	else
		snprintf(node_temp->label_end, sizeof(node_temp->label_end), "m'");
	node[0]->next = node_temp;
	*node = node_temp; 
}

int solve_cmd(char * buf, FILE * fp, S_ORDER_NODE ** node);

int solve_sequence(char* buf, S_ORDER_NODE ** node)
{
	S_ORDER_NODE * node_temp = NULL;
	int label_num = 0;
	char *c_temp = NULL;
	if(buf == NULL || node == NULL)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}

	label_num = atoi(buf);
	c_temp = strstr(buf, ";");
	if(c_temp == NULL)
	{
		printf("[%s,%d],buf err:%s\r\n", __FUNCTION__, __LINE__, buf);
		return -1;
	}
	*c_temp = 0;
	c_temp = strstr(buf, ":");
	if(c_temp == NULL)
	{
		printf("[%s,%d],buf err:%s\r\n", __FUNCTION__, __LINE__, buf);
		return -1;
	}
	c_temp++;
	add_node(node, label_num, label_num+1, c_temp, E_EXP_TYEP_SEQUENCE);
	return 0;
}

int solve_branch(char * buf, FILE * fp, S_ORDER_NODE ** node)
{
	int l_if = 0;
	int l_else = 0;
	int l_end = 0;
	char * p_if = NULL;
	char * p_then = NULL;

	char revert_buf[MAX_BUF_SIZE];
	char read_buf[MAX_BUF_SIZE];
	if(buf == NULL || fp == NULL || node == NULL)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	l_if = atoi(buf);
	p_if = strstr(buf, "if");
	p_then = strstr(buf, "then");
	if(!p_then || !p_if)
	{
		printf("[%s,%d],bad_input, %s\r\n", __FUNCTION__, __LINE__, buf);
		return -1;
	}
	*p_then = 0x00;
	p_if += strlen("if");
	add_node(node, l_if, l_if+1, p_if, E_EXP_TYPE_BRANCH);
	while(fgets(read_buf, sizeof(read_buf), fp))
	{
		if(strstr(read_buf, "else"))
		{
			l_else = atoi(read_buf);
		}
		else if(strstr(read_buf, "endif"))
		{
			l_end = atoi(read_buf);
			break;
		}
		else
		{
			solve_cmd(read_buf, fp, node);
		}
	}
	snprintf(revert_buf, sizeof(revert_buf), "!(%s)", p_if);
	add_node(node, l_if, l_else+1, revert_buf, E_EXP_TYPE_BRANCH);
	add_node(node, l_else, l_end + 1, "_", E_EXP_TYPE_BRANCH);
	add_node(node, l_end, l_end+1, "_", E_EXP_TYPE_BRANCH);
	return -1;
}

int solve_circulation(char * buf, FILE * fp, S_ORDER_NODE ** node)
{
	int l_while = 0;
	int l_endwhile = 0;
	char *p_while = NULL;
	char * p_then = NULL;
	char revert_buf[MAX_BUF_SIZE];
	char read_buf[MAX_BUF_SIZE];
	if(buf == NULL || fp == NULL || node == NULL)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	l_while = atoi(buf);
	p_while = strstr(buf, "while");
	p_while += strlen("while");
	p_then = strstr(buf, "then");
	if(!p_then || !p_while)
	{
		printf("[%s,%d],bad_input, %s\r\n", __FUNCTION__, __LINE__, buf);
		return -1;
	}
	*p_then = 0x00;
	add_node(node, l_while, l_while+1, p_while, E_EXP_TYPE_CIRCULATION);
	while(fgets(read_buf, sizeof(read_buf), fp))	
	{
		if(strstr(read_buf, "endwhile"))
		{
			l_endwhile = atoi(read_buf);
			break;
		}
		else
		{
			solve_cmd(read_buf, fp, node);
		}
	}
	snprintf(revert_buf, sizeof(revert_buf), "!(%s)", p_while);
	add_node(node, l_while, l_endwhile, revert_buf, E_EXP_TYPE_BRANCH);
	add_node(node, l_endwhile, l_endwhile + 1, "_", E_EXP_TYPE_BRANCH);


}

int solve_cmd(char * buf, FILE * fp, S_ORDER_NODE ** node)
{
	if(buf == NULL || fp == NULL || node == NULL)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}	
	printf("[%s,%d],buf:%s\r\n", __FUNCTION__, __LINE__, buf);
	while(node[0]->next != NULL)
		*node = node[0]->next;
	if(strstr(buf, ";"))
	{
		solve_sequence(buf, node);
	}
	else if(strstr(buf, "if"))
	{
		solve_branch(buf, fp, node);
	}
	else if(strstr(buf, "while"))
	{
		solve_circulation(buf, fp, node);
	}
}


int info_delete_space(char * src, char * target, int target_size)
{
	int src_offset = 0;
	int tag_offset = 0;
	if(src == NULL || target == NULL || target_size == 0)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}

	while(src[src_offset] != 0x00 && tag_offset < target_size )
	{
		if(src[src_offset] != ' ')
		{
			target[tag_offset] = src[src_offset];
			tag_offset++;
		}
		src_offset++;
	}
	return 0;
}

int imppre(char * src_file, char* target_file)
{
	int ret  = -1;
	FILE * fp_imp = NULL;
	FILE * fp_lable = NULL;
	char read_buf[MAX_BUF_SIZE];
	char temp_buf[MAX_BUF_SIZE];
	char write_buf[MAX_BUF_SIZE];
	int i = 1;
	if(src_file == NULL || target_file == NULL)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		goto _end;
	}	
	fp_imp = fopen(src_file, "r");
	if(fp_imp == NULL)
	{
		printf("[%s,%d],src open fail:%s\r\n", __FUNCTION__, __LINE__, src_file);
		goto _end;
	}
	fp_lable = fopen(target_file, "w");
	if(fp_lable == NULL)
	{
		printf("[%s,%d],tag open fail:%s\r\n", __FUNCTION__, __LINE__, target_file);
		goto _end;
	}
	while(fgets(read_buf, MAX_BUF_SIZE, fp_imp))
	{
		memset(temp_buf, 0, sizeof(temp_buf));
		if(info_delete_space(read_buf,temp_buf, sizeof(temp_buf)))
		{
			printf("[%s,%d],info_delete_space err\r\n", __FUNCTION__, __LINE__);
			goto _end;
		}
		snprintf(write_buf, sizeof(write_buf), "%d:%s", i, temp_buf);
		fprintf(fp_lable, "%s", write_buf);
		i++;
	}
	end_label = i;
	ret = 0;

_end:
	if(fp_imp != NULL)
		fclose(fp_imp);
	if(fp_lable != NULL)
		fclose(fp_lable);
	return ret;
}

int output_json_init(char *val, FILE * fp)
{
	cJSON * root = NULL;
	cJSON * payloaddata = NULL;
	cJSON * initval = NULL;
	int i = 0;
	char *buf;
	char temp_str[2] = {0,0};
	if(val == NULL || fp == NULL)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	root  = cJSON_CreateObject();
	payloaddata = cJSON_CreateObject();
	initval = cJSON_CreateArray();



	cJSON_AddItemToObject(payloaddata, "initialValue", initval);
	cJSON_AddItemToObject(root, "payloadData", payloaddata);
	cJSON_AddItemToObject(root, "pcFirst", cJSON_CreateString("m"));
	cJSON_AddItemToObject(root, "pcLast", cJSON_CreateString("L1"));
	for(i = 0; i < 26; i++)
	{
		if(val[i] != 0)
		{
			cJSON * item = cJSON_CreateObject();
			temp_str[0] = 'a' + i;
			cJSON_AddStringToObject(item, "name", temp_str);
			cJSON_AddStringToObject(item, "value", "0");
			cJSON_AddItemToArray(initval, item);
		}
	}


	buf = cJSON_Print(root);
	cJSON_Delete(root);
	if(buf)
	{
		printf("%s", buf);
		fprintf(fp, "%s,\r\n",buf);
		free(buf);
	}
	else
	{
		printf("[%s,%d],cjson output error\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	
	
	return 0;
}

int output_json_node(S_ORDER_NODE * node, FILE * fp)
{
	cJSON * root = NULL;
	cJSON * payloaddata = NULL;
	cJSON * exp = NULL;
	cJSON * same = NULL;
	char * buf = NULL;
	char temp_str[2] = {0,0};
	if(node == NULL || fp == NULL)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}

	while(node != NULL)
	{
		if(node->label_start[0] == 'm')
		{
			node = node->next;
			continue;
		}

		root  = cJSON_CreateObject();
		payloaddata = cJSON_CreateObject();
		exp = cJSON_CreateObject();
		cJSON_AddItemToObject(root, "payloadData", payloaddata);
		cJSON_AddItemToObject(root, "pcFirst", cJSON_CreateString(node->label_start));
		cJSON_AddItemToObject(root, "pcLast", cJSON_CreateString(node->label_end));
		cJSON_AddItemToObject(payloaddata, "exp", exp);
		cJSON_AddItemToObject(exp, "expression", cJSON_CreateString(node->exp));
		if(node->div == 0)
		{
			cJSON_AddItemToObject(exp, "same", cJSON_CreateString("same"));
		}
		else
		{
			same = cJSON_CreateObject();
			cJSON_AddItemToObject(exp, "same", same);
			temp_str[0] = node->div;
			cJSON_AddItemToObject(same, "div", cJSON_CreateString(temp_str));
		}
		buf = cJSON_Print(root);
		cJSON_Delete(root);
		if(buf != NULL)
		{
			if(node->next != NULL)
				fprintf(fp, "%s,\r\n", buf);
			else
				fprintf(fp, "%s\r\n", buf);
			
		}
		else
		{
			printf("[%s,%d],cjson output error\r\n", __FUNCTION__, __LINE__);
			return -1;		
		}
		node = node->next;
		
	}
	return 0;
}


int output_json(char * val, S_ORDER_NODE *node)
{
#define ORDER_JSON_FILE		"order.json"

	FILE * fp = NULL;
	char * temp = NULL;
	if(val == NULL || node == NULL)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	fp = fopen(ORDER_JSON_FILE, "w");
	if(fp == NULL)
	{
		printf("[%s,%d],ORDER_JSON_FILE open fail\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	fprintf(fp, "[\r\n");
	output_json_init(val, fp);
	output_json_node(node, fp);
	fprintf(fp ,"]");
	fclose(fp);
	return 0;
}

int store_order(S_ORDER_NODE *node)
{
	FILE * fp = NULL;

	if(node == NULL)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	fp = fopen(ORDER_FILE, "w");
	if(fp == NULL)
	{
		printf("[%s,%d],open fail\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	while(node != NULL)
	{
		if(node->exp[0] == 0x00)
		{
			fprintf(fp,"pc=%s & pc'=%s\r\n", node->label_start, node->label_end);
		}
		else
		{
			if(node->div == 0)
				fprintf(fp, "pc=%s & pc'=%s & %s\r\n", node->label_start, node->label_end, node->exp);
			else
				fprintf(fp, "pc=%s & pc'=%s & %s & same(V/%c)\r\n", node->label_start, node->label_end, node->exp, node->div);
		}
		
		
		node = node->next;
	}
	fclose(fp);
	return 0;
}

int show_info(S_ORDER_NODE * node)
{
	int i = 0;
	printf("SHOW VAL\r\n");
	for(i = 0; i < 26; i++)
	{
		if(val[i] != 0)
			printf("%c ", 'a' + i);
	}
	printf("\r\n");
	printf("SHOW_NODE\r\n");
	while(node != NULL)
	{
		if(node->exp[0] == 0x00)
		{
			printf("pc=%s & pc'=%s\r\n", node->label_start, node->label_end);
		}
		else
		{
			if(node->div == 0)
				printf("pc=%s & pc'=%s & %s\r\n", node->label_start, node->label_end, node->exp);
			else
				printf("pc=%s & pc'=%s & %s & same(V/%c)\r\n", node->label_start, node->label_end, node->exp, node->div);
		}
		
		
		node = node->next;
	}
	printf("SHOW NODE END\r\n");
}

int main(int argc, char* argv[])
{
	char* imp_path = NULL;
	char read_buf[MAX_BUF_SIZE];
	FILE * fp = NULL;
	S_ORDER_NODE * order_temp = &order_node_head;
	
	memset(val, 0 ,sizeof(val));
	printf("argc:%d\r\n", argc);
	if(argc != 2)
	{
		printf("[%s,%d],bad_input\r\n", __FUNCTION__, __LINE__);
		return -1;
	}	
	imp_path = argv[1];
	printf("imp_path:%s\r\n", imp_path);

	if(imppre(imp_path, LABEL_FILE))
	{
		printf("[%s,%d],imppre err\r\n", __FUNCTION__, __LINE__);
		return -1;
	}
	fp = fopen(LABEL_FILE, "r");
	if(fp == NULL)
	{
		printf("[%s,%d],open LABEL FILE err\r\n", __FUNCTION__, __LINE__);
		return -1;
	}

	memset(order_temp, 0, sizeof(S_ORDER_NODE));
	order_node_head.label_start[0] = 'm';
	order_node_head.label_end[0] = 'l';
	order_node_head.label_end[1] = '1';

	while(fgets(read_buf, MAX_BUF_SIZE, fp))
	{
		solve_cmd(read_buf, fp, &order_temp);
	}
	show_info(&order_node_head);
	store_order(&order_node_head);
	output_json(val, &order_node_head);
	return 0;
}
