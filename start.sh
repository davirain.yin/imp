cd code/

echo "Run parse imp language"

gcc imp_order.c cJSON.c cJSON.h -lm

./a.out imp.imp

cp order.json ../imp-interpeter/input/

cd ..

echo "Run Imp interpeter"

cd imp-interpeter/ 

cargo run --release  -- ./input/order.json  ./output -o FILE

cd .. 
#mkdir ./output
#mkdir -r ../output && cp ./output/multi_* ../output 

cp -r ./imp-interpeter/output/multi_* ./output

echo "Run picture"

gcc demo.cpp

./a.out ./output/multi_s.txt ./output/multi_r.txt KS.dot


dot KS.dot  -T png -o ks.png

open ks.png
